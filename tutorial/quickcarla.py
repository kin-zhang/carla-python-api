#!/usr/bin/env python


from __future__ import print_function

# ==============================================================================
# -- find carla module ---------------------------------------------------------
# ==============================================================================

import glob
import os
import sys
from tkinter import E

try:
    # print(os.path.abspath(__file__))
    # print(os.path.dirname(os.path.abspath(__file__)))
    sys.path.append(glob.glob(os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + '/carla/dist/carla-*%d.%d-%s.egg' % (
        sys.version_info.major,
        sys.version_info.minor,
        'win-amd64' if os.name == 'nt' else 'linux-x86_64'))[0])
except IndexError:
    pass
# ==============================================================================
# -- imports -------------------------------------------------------------------
# ==============================================================================

import carla
import argparse

description = "Benchmark CARLA performance in your platform for different towns and sensor or traffic configurations.\n"

parser = argparse.ArgumentParser(description=description)
parser.add_argument('--host', default='localhost', help='IP of the host server (default: localhost)')
parser.add_argument('--port', default=2000, help='TCP port to listen to (default: 2000)')
parser.add_argument('--rolename', default='hero', help='actor role name (default: "hero")')
args = parser.parse_args()

# ==============================================================================
# -- user imports --------------------------------------------------------------
# ==============================================================================

import pygame as pg
from pygame.locals import * # import keyboard
from numpy import random
import numpy as np

from utils.color import bcolors

class Status:
    def __init__(self, args) -> None:
        self.spectator_status_set = False
        self.draw_set = False
        self.draw_all = False
        self.time_draw = 7
        self.client = carla.Client(args.host, args.port)
        self.client.set_timeout(5.0)
        # retrieve the world that currently running
        self.world = self.client.get_world()
        self.vehicles_list = self.world.get_actors().filter('vehicle.*')
        self.find_ego_car()

    def find_ego_car(self) -> None:
        # find all things in world
        hero_vehicles = [actor for actor in self.world.get_actors()
                            if 'vehicle' in actor.type_id and actor.attributes['role_name'] == args.rolename]
        self.ego_vehicle = None
        if len(hero_vehicles) > 0:
            self.ego_vehicle = random.choice(hero_vehicles)
            self.hero_transform = self.ego_vehicle.get_transform()
            print(f"{bcolors.OKGREEN}Select from length:{len(hero_vehicles)}, find ego car id:{self.ego_vehicle.id}{bcolors.ENDC}")
        else:
            print(f"{bcolors.FAIL}!! CANNOT find the ego car, please check the rolename as:{args.rolename}, pls try again by pressing keyboard 'r'{bcolors.ENDC}")
    
    def update(self):
        if self.ego_vehicle:
            etransform = self.ego_vehicle.get_transform()
            ebb = self.ego_vehicle.bounding_box
            ebb.location+=etransform.location
            if self.spectator_status_set:
                spectator = self.world.get_spectator()
                spectator.set_transform(carla.Transform(etransform.location + carla.Location(z=50),
                                                        carla.Rotation(pitch=-90)))
            if self.draw_set:
                if self.draw_all:
                    for actor in self.vehicles_list:
                        vel = actor.get_velocity()
                        atran = actor.get_transform()
                        bb = actor.bounding_box
                        bb.location += atran.location
                        for t in range(0,self.time_draw,2):
                            bb.location += vel*t
                            self.world.debug.draw_box(bb, atran.rotation, 0.01, carla.Color(255,255,0,0), self.time_draw/5)
                    # return
                else:
                    self.world.debug.draw_box(ebb, etransform.rotation, 0.05, carla.Color(255,0,0,125),1)

        for event in pg.event.get():
            if event.type == pg.KEYDOWN:
                if event.key == pg.K_s:
                    self.spectator_status_set = not self.spectator_status_set
                    print("Setting spectator to car: ", self.spectator_status_set)
                elif event.key == pg.K_p:
                    # print info you like
                    print('position:',etransform.location,'bounding box:', ebb) # ,'rotation:',etransform.rotation
                elif event.key == pg.K_d:
                    # draw info
                    self.draw_set = not self.draw_set
                    print("Setting drawing the bounding box:", self.draw_set)
                elif event.key == pg.K_a:
                    # draw info
                    self.draw_all = not self.draw_all
                    print(f"Setting drawing all vehicle:{self.draw_all}, pls press keyboard '{bcolors.OKCYAN}d{bcolors.ENDC}' for drawing")
                elif event.key == pg.K_r:
                    print(f"{bcolors.OKCYAN}finding the ego car again... {bcolors.ENDC}")
                    self.find_ego_car()
                
def main(args):
    pg.init()
    screen = pg.display.set_mode((150, 150))
    # img = pg.image.load(args.img)
    my_status = Status(args)
    try:
        while True:
            my_status.update()
            # screen.blit(img, (1, 1))
            pg.display.flip()
    except Exception as e:
        print(bcolors.FAIL + e + bcolors.ENDC)
    except KeyboardInterrupt:
        print('\nCancelled by user. Bye!')

if __name__ == '__main__':
    main(args)