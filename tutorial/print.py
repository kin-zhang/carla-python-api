"""
此代码主要用于连接到客户端进行一些消息的打印
"""

import glob
import os
import sys

# ==============================================================================
# -- Find CARLA module ---------------------------------------------------------
# ==============================================================================
try:
    sys.path.append(glob.glob('../carla/dist/carla-*%d.%d-%s.egg' % (
        sys.version_info.major,
        sys.version_info.minor,
        'win-amd64' if os.name == 'nt' else 'linux-x86_64'))[0])
except IndexError:
    pass

import carla
import math


try:
    
    client = carla.Client('localhost', 3000)
    client.set_timeout(3.0)
    world = client.get_world()
    world_map = world.get_map()
    vehicles_list = world.get_actors().filter('vehicle.*')
    weather = carla.WeatherParameters(
    cloudiness=80.0,
    precipitation=30.0,
    sun_altitude_angle=90.0)
    # world.set_weather(weather)
    for v in vehicles_list:
        # print(v.get_transform())
        if v.attributes['role_name'] == "hero":
            hero_vehicle = v # Actor

    # spectator = world.get_spectator()
    # print('previous',spectator.get_transform())
    # spectator.set_transform(carla.Transform(carla.Location(x=80,y=200,z=200),carla.Rotation(yaw=0,pitch=-90,roll=0)))
    # print('now',spectator.get_transform())
    while True:
        world.wait_for_tick()
        for v in vehicles_list:
            if v.attributes['role_name'] != "hero":
                distance = math.sqrt((hero_vehicle.get_transform().location.x-v.get_transform().location.x)**2 + (hero_vehicle.get_transform().location.y - v.get_transform().location.y)**2)
                if distance<20 and (int(v.get_velocity().y) or int(v.get_velocity().x) or int(v.get_velocity().z) != 0):
                    v_speed = v.get_velocity()
                    speed_kmh = (3.6 * math.sqrt(v_speed.x**2 + v_speed.y**2 + v_speed.z**2))
                    # print('type:',v.type_id ,'id',v.id,'speed:', speed_kmh)
        # spectator.set_transform(carla.Transform(carla.Location(x=80,y=200,z=200),carla.Rotation(yaw=0,pitch=-90,roll=0)))
        print('position:',hero_vehicle.get_transform().location) # ,'rotation:',hero_vehicle.get_transform().rotation
        # print('vehicle waypoint',world_map.get_waypoint(hero_vehicle.get_transform().location).lane_id)
        # print(hero_vehicle.get_control().throttle)

except KeyboardInterrupt:
        print('\nCancelled by user. Bye!')
