# CARLA 学习

此repo进行汇总所有的东西

文件夹架构：

1. carla 为carla自带的一些东西
2. examples 为carla自己给出的一些使用样例

3. tutorial 为本分支自己的一些代码，命名方式基本都表明了用途
4. util 没咋用过

## 系统

Window和Ubuntu 18.04 混用的
Carla版本：0.9.10.1/0.9.11
Python版本：python 3.7 （建议conda管理python版本进行python版本切换与安装包管理）

- Window因为2000口本占用了，所以都是3000的口

- Ubuntu一般都是正常默认2000口

## 使用

安装Python所需：

```bash
conda create -n py37 python=3.7
conda activate py37
pip3 install -r requirements.txt
```

每个文件都指明了相关的用途

---
### quickly-carla

转成了一个GUI工具

Github: [https://github.com/Kin-Zhang/quickly-carla](https://github.com/Kin-Zhang/quickly-carla)

Gitee: [https://gitee.com/kin_zhang/quickly-carla](https://gitee.com/kin_zhang/quickly-carla)

bilibili: [https://www.bilibili.com/video/BV1hG4y1m7TD/](https://www.bilibili.com/video/BV1hG4y1m7TD/)

### ~quickcarla.py 用法~

```python
 python tutorials/quickcarla.py --port 2000
```

其中`quickcarl.py` 为自己快速使用的carla debug工具，为出现一个pygame界面，点击相关键盘操作即可触发相关设置，例如：

1. 按键盘`S`，即可把CARLA主视角切换到ego car顶端，再按一下即可取消
2. 字母`D`，画框框
3. 字母`P`，打印一些你想打印的东西，具体可以往里面填代码
4. 字母`R`，在没有找到ego car的时候，检查一下rolename 重新再找一下，一般CARLA自身的ego car的rolename为`hero`，ROS那边则为`ego_vehicle`

测试截图：

![](docs/examples.png)
